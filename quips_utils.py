# Copyright (C) 2022 The Qt Company Ltd.
# Contact: https://www.qt.io/licensing/
#
# You may use this file under the terms of the CC0 license.
# See the file LICENSE.CC0 from this package for details.

import re
from pathlib import Path
from dash import dcc, html
from web_utils import get_header, get_column, get_file_content
import dash_dangerously_set_inner_html


def get_quip_number(s):
    return int(re.findall(r"\d{4}", s)[0])


def replace_quip_url(s):
    # quip-0001.html -> /quips/1
    quip_links = re.findall(r"quip-\d{4}-?[\w-]*.html", s)
    replacements = {}
    for old in quip_links:
        if old not in replacements:
            number = get_quip_number(old)
            replacements[old] = f"/quips/{number}"

    for k, v in replacements.items():
        s = s.replace(k, v)
    return s


def get_quips_index():
    return """index"""


def get_html_content(fname):
    content = ""
    with open(fname) as f:
        content = f.read()

    content = replace_quip_url(content)
    return content


def get_quip_html(html_file):
    return html.Div(
        [
            get_header(),
            html.Div(
                children=[],
                style={"height": "10px"},
                className="row",
            ),
            html.Div(
                children=[
                    html.Div(
                        dash_dangerously_set_inner_html.DangerouslySetInnerHTML(
                            get_html_content(html_file)
                        ),
                    ),
                ],
                className="row justify-center",
            ),
        ]
    )


def get_quip_layout_from_file(quip_file):
    if not Path(quip_file).exists():
        raise Exception(f"File '{quip_file}' do not exist")

    return html.Div(
        [
            get_header(),
            html.Div(
                children=[],
                style={"height": "10px"},
                className="row",
            ),
            html.Div(
                children=[
                    get_column(
                        divs=[get_quips_index()],
                        columns_number="three",
                    ),
                    get_column(
                        divs=[get_file_content(quip_file)],
                        columns_number="seven",
                    ),
                ],
                className="row justify-center",
            ),
        ]
    )


def resolve_quips_url(numbers, pathname):
    if not pathname.startswith("/quips/"):
        return False

    number = pathname.replace("/quips/", "")
    try:
        n = int(number)
    except ValueError:
        return False

    if n not in numbers:
        return False

    return True
