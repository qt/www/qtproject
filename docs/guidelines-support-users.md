#### Support Users

Helpful people are at the center of every great community, and everyone needs a bit of
help sometimes.
Whether it is a specific programming problem or more general guidance required,
there are many ways to share knowledge and support others.

* [Forum](http://forum.qt.io)
* [Online communities](http://wiki.qt.io/OnlineCommunities)
