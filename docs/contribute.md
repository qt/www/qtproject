### How to Contribute

> The Qt project is actively looking for new contributors. Even if you are
> starting with Qt, or have C++, QML, Python interest, we encourage to
> contribute! As a first-timer contributor, you will get reviewers assigned
> automatically to your first patch so they can help you.

The following steps are intended for code and documentation
contributions, but keep in mind you can help the project in many
other ways.

#### Setting up your environment

##### 1. Create an Account

[Register](https://login.qt.io/register) your Qt account, so you can
easily authenticate among all the Qt services: Bug tracker, Gerrit, Wiki,
etc.

##### 2. Install the necessary software

You can check the platform-dependent requirements for building Qt from source
in the [documentation page](https://doc.qt.io/qt-6/build-sources.html), here is
a short list:
* [Git](https://git-scm.com) is being use in all the repositories,
  so you will need it for getting the source code, push your contributions,
  etc.
* [CMake](https://cmake.org) is used for configuring a Qt build.
* [Ninja](https://ninja-build.org) is recommended in order to compile the
  project that is configured by CMake.

##### 3. Get the sources

You can get the
[`qtbase` repository](https://code.qt.io/cgit/qt/qtbase.git/tree/)
as a starting point which include many modules like Core, GUI, and Widgets:
```bash
git clone https://code.qt.io/qt/qtbase.git
```
All the `qtbase` modules can be
[found here](https://code.qt.io/cgit/qt/qtbase.git/tree/src).
Other Qt modules can be [found here](https://code.qt.io/cgit/qt/qt5.git/tree/).

In order to initialize this meta repository, and get all the sub-modules you can
use the `init-repository` Perl script. There are a few options in case you want
to only initialize a few sub-modules, on specific branches [read more
here](https://wiki.qt.io/Building_Qt_5_from_Git#Getting_the_submodule_source_code)

##### 4. Configure Gerrit

Login into [Gerrit](https://codereview.qt-project.org/) and add your desire
information to your account, but remember to accept the
[CLA](https://www.qt.io/legal-contribution-agreement-qt).

Configure your SSH and add the proper remote to your repository.
Read more about the process [on the wiki](https://wiki.qt.io/Setting_up_Gerrit)
about the process, but if you configured everything correctly, you should
get the following message when trying the following command:
```
$ ssh codereview.qt-project.org
  ****    Welcome to Gerrit Code Review    ****

  Hi <name>, you have successfully connected over SSH.

  Unfortunately, interactive shells are disabled.
  To clone a hosted Git repository, use:

  git clone ssh://<username>@codereview.qt-project.org:29418/REPOSITORY_NAME.git

Connection to codereview.qt-project.org closed.
```

#### Prepare your contributions

##### 1. Find an issue to solve

In case you are not solving a problem you encounter while developing an
application, you can check the list of open issues in
[our bug report system](https://bugreports.qt.io/projects/QTBUG)
in order to find something that is interesting to you.

##### 2. Commit & Push!

Once you have your local patch ready to submit, remember a few things:

Check the [Commit Policy](https://wiki.qt.io/Commit_Policy)
and remember to add a `Task-number:` or `Fixes:` entry to the commit footer.
Since your patch will be pushed to the `dev` branch,
add a `Pick-to:` when a cherry pick to another branch if necessary.

```
git add <the files you modified>
git commit  # this will open an editor, for you to write a message
git push gerrit HEAD:refs/for/dev
```

If the `push` worked well, you will get a URL for your new change
on [Gerrit](https://codereview.qt-project.org) where you will be able
to modify the commit message with the web interface, add reviewers,
and view your change in detail.

#### Need help?

You can get support from the community on various platforms
and website, like our [Forum](https://forum.qt.io/), [Mailing
Lists](https://lists.qt-project.org/), and other [Online
communities](http://wiki.qt.io/OnlineCommunities).
