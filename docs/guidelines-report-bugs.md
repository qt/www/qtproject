#### Report Bugs

Meaningful [bug reports](http://wiki.qt.io/ReportingBugsInQt) and comments help
improve the quality of Qt in a very direct way, while voting and
[triaging](http://wiki.qt.io/Triaging_Bugs) help to prioritize tasks.  We
encourage Qt users and Qt contributors to join efforts on the bug tracker.
