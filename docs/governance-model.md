### Governance model

The main objectives of the Governance Model are to:

* Put decision power in the hands of the community,
  i.e. the people who contribute to the Project's success
* Make it easy to understand how to get involved and make a difference

The five levels of involvement: Users, Contributors, Approvers, Maintainers and Chief Maintainer
([read more](http://wiki.qt.io/The_Qt_Governance_Model)).
